﻿Anfang und Ende reichen einander die Hände.
Arbeit bringt Brot, Faulenzen Hungersnot.
Auch dem geschickten Weber reisst einmal der Faden.
Auf der Reise ein guter Gefährt ist so gut wie ein Pferd.
Aufgeschoben ist nicht aufgehoben.
Ausnahmen bestätigen die Regel.
Bär bleibt Bär, fährt man ihn auch über das Meer.
Besser zehn Neider denn ein Mitleider.
Böse Beispiele verderben gute Sitten.
Daheim ist der Himmel blauer und die Bäume grüner.
Das Gerücht tötet den Mann.
Das ist der grösste Narr von allen, der allen Narren will gefallen.
Das schlechteste Rad knarrt am meisten.
Das sind die Weisen, die vom Irrtum zur Wahrheit reisen.
Das taugt weder zum Sieden noch zum Braten.
Das Wasser der kleinen Bäche bildet den grossen Strom
Der Esel und die Nachtigall haben beide ungleichen Schall.
Der Kuckuck ruft seinen eigenen Namen.
Der Lahme findet überall Krücken.
Der Schwatzhafte schadet sich selbst.
Der Speck lässt von der Schwarte nicht.
Der Tod hat noch keinen vergessen.	
Der Tod lauert überall, er kommt zu Fest und Ball.
Der vielerlei beginnt, gar wenig Dank gewinnt.
Edel macht das Gemüt, nicht das Geblüt.
Ehrlicher Feind ist besser als ein falscher Freund.
Ein alter Mann, der freit, ist nicht gescheit.
Ein Bild ist besser als tausend Wörter.
Ein finstrer Blick kommt finster zurück.
Ein grosses Schiff braucht ein grosses Fahrwasser.
Ein Unglück bietet dem andern die Hand.
Faulem Arbeiter ist jeder Hammer zu schwer.
Feindes Gaben gelten nicht.
Fleiss bringt Brot, Faulheit Not.
Hab`ich ist ein schöner Vogel, Hätt`ich nur ein Nestling.
Halt aus und du wirst Wunder sehen.
Halt Mass im Salzen, doch nicht im Schmalzen.
Handwerk hat einen goldenen Boden.
danke, dass Sie heruntergeladen wurde unser Spiel.
schauen Sie in unsere anderen Projekte.
Hungriger Bauch lässt sich mit Worten nicht abspeisen.
Hüte dich vor den Katzen, die vorne lecken und hinten kratzen.
Hundert Schläge auf fremdem Rücken sind nicht viel
Im Hause des Gehängten rede nicht vom Stricke.
Im schönsten Apfel sitzt der Wurm.
In der Not backt man aus Steinen Brot.
Kannst du Karre schieben, kannst du Arbeit kriegen.
Katze aus dem Haus, rührt sich die Maus.
Kein Mädchen ohne Liebe, kein Jahrmarkt ohne Diebe.
Kein Vorteil ohne Nachteil.
Kinder und Narren sagen immer die Wahrheit.
Kleine Kinder, kleine Sorgen, grosse Kinder, grosse Sorgen
Kurzer Flachs gibt auch langen Faden.
Gwenderon zügelte sein Pferd.
Karelian drehte sich wortlos um und verschwand im Unterholz.
Er verscheuchte den Gedanken.
Aber er sollte nicht auf Gerüchte horen.
"Ein Raett?", sagte er erschrocken. "Hier?"
"Und … was schließt du daraus?
Trotzdem fühlte er die Anspannung.
Sommer braucht bestimmt einen zusätzlichen Monat.
Niemand weiß viel über diesen Teil der Walder.
Hilf mir diese tödliche Liebe zu überleben.
Morgen, morgen, nur nicht heute, sagen alle faulen Leute.
Wer zufrieden ist, ist glücklich.
Wer es nicht im Kopfe hat, hat es in den Beinen.
Der Säufer schläft seinen Rausch aus, der Tor aber nie.
Liebe macht blind.
Das Wasser ist König, sogar das Feuer hat Angst vor dem Wasser.
Wer Wind sät, wird Sturm ernten.
Ende gut, alles gut.
Wer zufrieden ist, ist glücklich.