﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEditor;
using System.IO;
using LFGameCore;
using UnityEngine.UI;

public class FileReader : MonoBehaviour {
    //Menu
    public Image WhiteBoard;
    public Image MenuBackPanel;
    public Text BestScoreText;
    //Game
    public Image GameBackPanel;
    public Image LeftButtonFlag;
    
    public Image RightButtonFlag;
    public Text ScoreField;
    public Text TaskField;

    //GameOver
    public Image GameOverBackPanel;

    public Image CorrectFlag;
    public Text FinalTaskField;
    public Text FinalScore;

    public float alpha =1f;
    private bool toChangeAlpha = false;
    private int dalphaSign  = +1;
    Image fromScreen; 
    Image toScreen;

    System.Action VoidBetweenScreens;
    float alphaSpeed = 2f;
	// Use this for initialization
	void Start () {
        

        Initialize();
	}
	
	// Update is called once per frame
	void Update () {
        if(toChangeAlpha){
            print("hi-ho-ha");
            alpha += dalphaSign*Time.deltaTime*alphaSpeed;
            if(alpha >= 1 && dalphaSign > 0)
            {
                fromScreen.gameObject.SetActive(false);
                VoidBetweenScreens();
                toScreen.gameObject.SetActive(true);
                dalphaSign =-1;
            }
            if(alpha <=0 && dalphaSign < 0)
            {
                toChangeAlpha=false;
                dalphaSign=+1;
                WhiteBoard.gameObject.SetActive(false);
            }
            if(dalphaSign>0){
                WhiteBoard.color = new Color(toScreen.color.r,toScreen.color.g,toScreen.color.b,alpha);
            }else{
                WhiteBoard.color = new Color(fromScreen.color.r,fromScreen.color.g,fromScreen.color.b,alpha);
            }
        }
    }
    public void ChangeTheMode(int changingId){
        print("tapped");
        switch (changingId){
            case 0: fromScreen = MenuBackPanel; toScreen = GameBackPanel;VoidBetweenScreens = GameModeInit; alphaSpeed = 2f; break;
            case 1: fromScreen = GameBackPanel; toScreen = GameOverBackPanel; VoidBetweenScreens = GameModeGameOver; alphaSpeed=2f; break;
            case 2: fromScreen = GameOverBackPanel; toScreen = MenuBackPanel; VoidBetweenScreens = MenuMode; alphaSpeed=3f; break;
            case 3: fromScreen = GameOverBackPanel; toScreen = GameBackPanel; VoidBetweenScreens = GameModeInit; alphaSpeed=2f; break;
            case 4: fromScreen = GameBackPanel; toScreen = GameBackPanel; VoidBetweenScreens = GameModeTask; alphaSpeed=6f; break;
            default: return; break;
        }
        WhiteBoard.gameObject.SetActive(true);
        toChangeAlpha = true;
    }
    void Initialize(){

        //Countries initialization
        new Country("Russian", Resources.Load<Sprite>("Images/Flags/buttonflagRUS"));
        new Country("Ukrainian", Resources.Load<Sprite>("Images/Flags/buttonflagUKR"));
        new Country("Belarusian", Resources.Load<Sprite>("Images/Flags/buttonflagBRS"));

        new Country("English", Resources.Load<Sprite>("Images/Flags/buttonflagENG"));
        new Country("French", Resources.Load<Sprite>("Images/Flags/buttonflagFRA"));
        new Country("Italian",Resources.Load<Sprite>("Images/Flags/buttonflagITA"));
        new Country("Spanish", Resources.Load<Sprite>("Images/Flags/buttonflagSPN"));

        new Country("German", Resources.Load<Sprite>("Images/Flags/buttonflagGER"));
        new Country("Finnish", Resources.Load<Sprite>("Images/Flags/buttonflagFIN"));
        new Country("Swedish",Resources.Load<Sprite>("Images/Flags/buttonflagSWD"));

        new Country("Chinese", Resources.Load<Sprite>("Images/Flags/buttonflagCHN"));
        new Country("Japanese", Resources.Load<Sprite>("Images/Flags/buttonflagJAP"));
        new Country("Korean",Resources.Load<Sprite>("Images/Flags/buttonflagKOR"));

        Debug.Log("Countries loaded: "+Country.ListOfCounties.Count);
        foreach (Country c in Country.ListOfCounties.Values)
        {
            Debug.Log("Countries loaded: "+c);
        }

        //Backgrounds loading
        int numOfBackgrounds = 19;
        for (int i = 1; i <= numOfBackgrounds; i++)
        {
            Sprite backImage = Resources.Load<Sprite>("Images/Backgrounds/bg ("+i+")");
            LFTask.Backgrounds.Add(backImage);
        }
        Debug.Log("Backgrounds loaded: " + LFTask.Backgrounds.Count);

        //Tasks loading
        //Slavic
        List<string> slavicCountries = new List<string>{
            "Russian",
            "Belarusian",
            "Ukrainian"
        };
        List<string> pathsToTaskFilesSlavic = new List<string>{
            "Texts/Slavic/RUS",
            "Texts/Slavic/BEL",
            "Texts/Slavic/UKR"
        };
        int ind = 0;
        foreach(string path in pathsToTaskFilesSlavic)
        {
            foreach (string line in ((TextAsset)Resources.Load(path)).text.Split('\n'))
            {
                LFTask.AddTask("Slavic",slavicCountries[ind],new LFTask(line,Country.ListOfCounties[slavicCountries[ind]]));
            }
            ind++;
        }
        //Roman
        List<string> romanCountries = new List<string>{ 
            "English", 
            "French", 
            "Italian", 
            "Spanish" 
        };
        List<string> pathsToTaskFilesRoman = new List<string>{
            "Texts/Roman/ENG",
            "Texts/Roman/FRA",
            "Texts/Roman/ITA",
            "Texts/Roman/SPA"
        };
        ind = 0;
        foreach(string path in pathsToTaskFilesRoman)
        {
            foreach (string line in ((TextAsset)Resources.Load(path)).text.Split('\n'))
            {
                LFTask.AddTask("Roman",romanCountries[ind],new LFTask(line,Country.ListOfCounties[romanCountries[ind]]));
            }
            ind++;
        }

        //German
        List<string> germanCountries = new List<string>{ 
            "German", 
            "Finnish", 
            "Swedish"
        };
        List<string> pathsToTaskFilesGerman = new List<string>{
            "Texts/German/GER",
            "Texts/German/FIN",
            "Texts/German/SWD"
        };
        ind = 0;
        foreach(string path in pathsToTaskFilesGerman)
        {
            foreach (string line in ((TextAsset)Resources.Load(path)).text.Split('\n'))
            {
                LFTask.AddTask("German",germanCountries[ind],new LFTask(line,Country.ListOfCounties[germanCountries[ind]]));
            }
            ind++;
        }

        //Asian
        List<string> asianCountries = new List<string>{ 
            "Chinese", 
            "Japanese", 
            "Korean"
        };
        List<string> pathsToTaskFilesAsian = new List<string>{
            "Texts/Asian/CHN",
            "Texts/Asian/JAP",
            "Texts/Asian/KOR"
        };
        ind = 0;
        foreach(string path in pathsToTaskFilesAsian)
        {
            foreach (string line in ((TextAsset)Resources.Load(path)).text.Split('\n'))
            {
                LFTask.AddTask("Asian",asianCountries[ind],new LFTask(line,Country.ListOfCounties[asianCountries[ind]]));
            }
            ind++;
        }
        State.BestScore = PlayerPrefs.GetInt("BestScore");
        BestScoreText.text = "Best: "+State.BestScore;
        MenuMode();
    }
    public void MenuMode()
    {
        
        MenuBackPanel.GetComponent<Image>().sprite = LFTask.Backgrounds[Random.Range(0,19)];
            
    }
    public void LeftVar(){
        if (State.CurrentTask.Check(State.CurrentTask.LeftCountry))
        {
            Success();
        }
        else
        {
            State.LastCorrect = State.CurrentTask.RightCountry;
            Fail();
        }
    }
    public void RightVar(){
        if (State.CurrentTask.Check(State.CurrentTask.RightCountry))
        {
            Success();
        }
        else
        {
            State.LastCorrect = State.CurrentTask.LeftCountry;
            Fail();
        }
    }
    public void GameModeInit()
    {
        State.Score = 0;
        GameModeTask();
    }
    public void Success()
    {
        State.Score++;
        ChangeTheMode(4);
    }

    public void Fail()
    {
        ChangeTheMode(1);
        //GameModeGameOver();
    }
    public void GameModeTask()
    {
        State.CurrentTask = LFTask.GetRandomTask(); //GenerateTask();
        ScoreField.text = State.Score.ToString();
        Sprite background = LFTask.Backgrounds[Random.Range(0,19)];
        GameBackPanel.GetComponent<Image>().sprite = background;
        GameOverBackPanel.GetComponent<Image>().sprite = background;
        LeftButtonFlag.GetComponent<Image>().sprite = State.CurrentTask.LeftCountry.Image;
        LeftButtonFlag.GetComponentInChildren<Text>().text = State.CurrentTask.LeftCountry.Name;
        RightButtonFlag.GetComponent<Image>().sprite = State.CurrentTask.RightCountry.Image;
        RightButtonFlag.GetComponentInChildren<Text>().text = State.CurrentTask.RightCountry.Name;
        TaskField.text=State.CurrentTask.TaskText;
        Debug.Log("Task generated");

    }
    public void GameModeGameOver()
    {
        CorrectFlag.GetComponent<Image>().sprite = State.LastCorrect.Image;
        CorrectFlag.GetComponentInChildren<Text>().text = State.LastCorrect.Name;
        FinalTaskField.text = State.CurrentTask.TaskText;
        FinalScore.text = State.Score.ToString();
        State.BestScore = Mathf.Max(State.BestScore,State.Score);
        PlayerPrefs.SetInt("BestScore",State.BestScore);
        PlayerPrefs.Save();
    }
}
  