﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Adder : MonoBehaviour {

    public Text textField;
    public int DefaultValue;
    int Value;

	void Start () {
        Value = DefaultValue;
        textField.text = Value.ToString();
	}
	
	public void AddValue(int valueToAdd)
    {
        Value += valueToAdd;
        textField.text = Value.ToString();
    }
}
